# README #

SimpleSyndicate.Mvc.VersionHistoryController NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Mvc.VersionHistoryController

### What is this repository for? ###

* A version history controller and views for MVC that displays the version history.

### How do I get set up? ###

* Intall the NuGet package and the editor templates will be copied into your project.

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.mvc.versionhistorycontroller/issues?status=new&status=open
