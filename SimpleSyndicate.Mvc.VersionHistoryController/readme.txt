﻿SimpleSyndicate.Mvc.VersionHistoryController NuGet package.

Version history controller and views for an ASP.Net MVC applications that uses
SimpleSyndicate.Mvc to provide various functionality.

The controller has been installed in Controllers\Home\VersionHistory, and views have
been installed into Views\Home\VersionHistory.

For the controller to work, the following is required:
- MvcCodeRouting is installed and enabled to provide sensible routing; if you're not
  using this you'll need to set up the route to the controller yourself
- Configure your Dependency Injection solution to inject a concrete
  SimpleSyndicate.Repositories.IRepository<SimpleSyndicate.Models.VersionHistoryItem>
- The repository will provide the version history information

The Index view supports some customisation for your application -- by default it looks
for a partial view named _IndexHeader and, if it exists, renders this instead of the
default header content; have a look at Index.cshtml for the full details.

If you want to do more customisation than this, you'll need to start adding code to
the Index view, but be aware that you'll need to be sure you don't overwrite your
changes if you install an upgraded version of this package. You should think about
either submitting a change request for the functionality or need or using this
package as a template and creating your own version of it for your particular needs.
